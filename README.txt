This project contains some experiments for having greater control over how tooltips
work and shown, including the types of content which may be included in a tooltip,
but also, allowing perhaps multiple tooltips.

At the heart of this, is a reimplementation of the "QTipLabel" widget/component. This
is only defined locally in the Qt C++ codebase in "qt/src/gui/kernel/qtooltip.cpp". It
is used there as the actual widget which gets created/shown to draw a tooltip on the screen,
with instances getting created and destroyed by the methods of QToolTip (i.e. Qt's tooltip
API). Currently, we have to reimplement this as we don't have access to it in Python.

-- Joshua Leung (aligorith@gmail.com), 03 April 2013 