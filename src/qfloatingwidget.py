# This file implements a widget which can contain arbitrarily complex contents
# while looking and behaving like a tooltip (in terms of show/hide behaviour).
# The implementation here is based on porting the QTipLabel widget over from
# the Qt sources, which is used internally by Qt to implement its tooltips.
#
# Author: Joshua Leung

import sys
import os

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

##################################
# Floating widget which looks like a tooltip

class QFloatingWidget(qgui.QWidget):
	# ctor
	# < (parent): (QWidget) widget which owns this floater
	#           = None on Windows, due to control issues
	def __init__(self, parent=None):
		qgui.QWidget.__init__(self, parent, qcore.Qt.ToolTip)
		
		# internal helper structures
		self.hideTimer = qcore.QBasicTimer()
		self.expireTimer = qcore.QBasicTimer()
		
		# customise widget to look like a tooltip
		self.setForegroundRole(qgui.QPalette.ToolTipText)
		self.setBackgroundRole(qgui.QPalette.ToolTipBase)
		self.setPalette(qgui.QToolTip.palette()) # XXX: allow substitution from users?
		self.ensurePolished()
		
		self.setWindowOpacity(self.style().styleHint(qgui.QStyle.SH_ToolTipLabel_Opacity, None, self) / 255.0)
		self.setMouseTracking(True)
		
	# Widget Implementation Overrides ---------------
	
	# Draw tooltip background, then contents of tooltip
	# < evt: (QPaintEvent)
	def paintEvent(self, evt):
		# tooltip background
		p = qgui.QStylePainter(self)
		
		opt = qgui.QStyleOptionFrame()
		opt.init(self)
		
		p.drawPrimitive(qgui.QStyle.PE_PanelTipLabel, opt)
		p.end()
		
		# contents...
		qgui.QWidget.paintEvent(self, evt)
	
	
	# Adjust mask on resizing - used for transparency/shadow effects on some platforms
	# < evt: (QResizeEvent)
	def resizeEvent(self, evt):
		frameMask = qgui.QStyleHintReturnMask()
		option = qgui.QStyleOption()
		option.init(self)
		
		if (self.style().styleHint(qgui.QStyle.SH_ToolTip_Mask, option, self, frameMask)):
			self.setMask(frameMask.region)

		qgui.QWidget.resizeEvent(self, evt)
	
	# Event Handling Overrides -----------------------
	
	# Timer Event - for hiding the widget after a fixed period of time
	# < evt: (QTimerEvent)
	def timerEvent(self, evt):
		# properly hide widget if timers have expired
		if evt.timerId() in (self.hideTimer.timerId(), self.expireTimer.timerId()):
			# stop timers
			self.resetTimers()
			
			# hide
			self.hideTipImmediately()
		else:
			# otherwise, standard handling applies
			qgui.QWidget.timerEvent(self, evt)
			
	
	# Mouse-move within ourselves - abort any current timers...
	# < evt: (QMouseEvent)
	def mouseMoveEvent(self, evt):
		# cancel timers (for now)
		self.resetTimers()
		
		# pass on events now
		qgui.QWidget.mouseMoveEvent(self, evt)
		
	# Leaving widget...
	# < evt: (QEvent)
	def leaveEvent(self, evt):
		# schedule leaving, or else the hide timer from owner of widget
		# will fail as we've already killed it...
		self.hideTip()
		
		# pass on event now
		qgui.QWidget.leaveEvent(self, evt)
	
	# Public API ------------------------------------
	
	# Set the (global) placement of the widget on screen
	# < pos: (QPoint) the point on screen where we want the tooltip to be spawned from (no offset yet)
	# < w: (QWidget) the owner widget
	def placeTip(self, pos, w):
		# get screen bounds
		if sys.platform.startswith('darwin'):
			screen = qgui.QApplication.desktop().availableGeometry(self.getTipScreen(pos, w))
		else:
			screen = qgui.QApplication.desktop().screenGeometry(self.getTipScreen(pos, w))
			
		# add os-dependent offset to point
		p = pos
		p += qcore.QPoint(2, 
						  21 if os.name == 'nt' else 16)
		
		# clamp within bounds
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(p.x() - 4 + self.width()) # p.rx()
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(p.y() - 24 + self.height()) # p.ry()
			
		if p.y() < screen.y():
			p.setY(screen.y())
			
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(screen.x() + screen.width() - self.width())
			
		if p.x() < screen.x():
			p.setX(screen.x())
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(screen.y() + screen.height() - self.height())
		
		# move label to this global point
		self.move(p)
	
	# ...........................
	
	# Reset timers so that they are not ticking over...
	def resetTimers(self):
		self.hideTimer.stop()
		self.expireTimer.stop()
	
	# Start fading out the tooltip bubble to hide it
	def hideTip(self):
		if not self.hideTimer.isActive():
			self.hideTimer.start(300, self)
	
	
	# Force tooltip bubble to hide immediately, without fade out delay
	def hideTipImmediately(self):
		self.close()        # to trigger QEvent::Close which stops the animation
	
	# ...........................
		
	# Return which screen (on multi-screen setups) the tooltip appears on
	# < pos: (QPoint) point on screen where tooltip/widget appears
	# < widget: (QWidget) widget that tooltip appears for
	# > returns: (int) screen number/index
	#          >> -1 = if widget does not appear
	def getTipScreen(self, pos, widget):
		if qgui.QApplication.desktop().isVirtualDesktop():
			return qgui.QApplication.desktop().screenNumber(pos)
		else:
			return qgui.QApplication.desktop().screenNumber(widget)

##################################
# API

# Show floating widget for given widget
def showFloatingWidget():
	pass

##################################
