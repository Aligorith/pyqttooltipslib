# Python port of QTipLabel widget used internally by QToolTip
# to display tooltip bubbles on screen...

import sys
import os

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

####################################
# Basic Tooltip Label/Bubble Widget
#
# All other standard text-based labels are all based on this same 
# widget, but define extra behaviours such as when they get hidden
class QTipLabel(qgui.QLabel):
	# ctor
	# < text: (str)
	# < w: (QWidget)
	def __init__(self, text, w):
		qgui.QLabel(self, w, qcore.Qt.ToolTip)
		
		# own data
		self.widget = qgui.QWidget() # PORTINGFIX: widget(0)
		self.rect = qcore.QRect()
		
		self.hideTimer = qcore.QBasicTimer()   # XXX: do we need such a timer?
		self.expireTimer = qcore.QBasicTimer() # XXX: do we need such a timer?
		
		# customise label to look like a tooltip bubble
		self.setForegroundRole(qgui.QPalette.ToolTipText)
		self.setBackgroundRole(qgui.QPalette.ToolTipBase)
		self.setPalette(qgui.QToolTip.palette()) # XXX: allow substitution from users?
		self.ensurePolished()
		
		self.setMargin(1 + self.style().pixelMetric(qgui.QStyle.PM_ToolTipLabelFrameWidth, None, self))
		self.setFrameStyle(qgui.QFrame.NoFrame)
		self.setAlignment(qgui.Qt.AlignLeft)
		self.setIndent(1)
		
		#qApp.installEventFilter(self) # PORTINGFIX: qApp.installEventFilter(this)
		self.setWindowOpacity(self.style().styleHint(qgui.QStyle.SH_ToolTipLabel_Opacity, None, self) / 255.0)
		self.setMouseTracking(True)
		self.fadingOut = False
		
		self.reuseTip(text) # XXX: rename this method? It is really setText() + fix size
	
	# Widget Implementation Overrides ---------------
	
	# Draw tooltip background, then contents of tooltip
	# < evt: (QPaintEvent)
	def paintEvent(self, evt):
		# tooltip background
		p = qgui.QStylePainter(self)
		
		opt = qgui.QStyleOptionFrame()
		opt.init(self)
		
		p.drawPrimitive(qgui.QStyle.PE_PanelTipLabel, opt)
		p.end()
		
		# contents...
		qgui.QLabel.paintEvent(self, evt)
	
	
	# Adjust mask on resizing - used for transparency/shadow effects on some platforms
	# < evt: (QResizeEvent)
	def resizeEvent(self, evt):
		frameMask = qgui.QStyleHintReturnMask()
		option = qgui.QStyleOption()
		option.init(self)
		
		if (self.style().styleHint(qgui.QStyle.SH_ToolTip_Mask, option, self, frameMask)):
			self.setMask(frameMask.region)

		qgui.QLabel.resizeEvent(self, evt)
	
	# Public API ------------------------------------
	
	# Set text to be displayed and adjust size of widget
	# < text: (str) The text to display in the label.
	#               Either plain text or HTML/QML formatting
	def reuseTip(self, text):
		self.setWordWrap(qgui.Qt.mightBeRichText(text)) # XXX: we often have to hack this!
		self.setText(text)
		
		fm = qgui.QFontMetrics(self.font())
		extra = qcore.QSize(1, 0)
		# Make it look good with the default ToolTip font on Mac, which has a small descent.
		if (fm.descent() == 2 && fm.ascent() >= 11)
			extra.setHeight(extra.height() + 1) # PORTINGFIX: ++extra.rheight()
			
		self.resize(self.sizeHint() + extra)
		self.restartExpireTimer() # XXX: we have to be able to disable this...
	
	
	# Set the (global) placement of the widget on screen
	# < pos: (QPoint) the point on screen where we want the tooltip to be spawned from (no offset yet)
	# < w: (QWidget) the owner widget
	def placeTip(self, pos, w):
		# PORTINGFIX: stylesheet attachment stuff for parent - needed?
		
		# get screen bounds
		if sys.platform.startswith('darwin'):
			screen = qgui.QApplication.desktop().availableGeometry(self.getTipScreen(pos, w))
		else:
			screen = qgui.QApplication.desktop().screenGeometry(self.getTipScreen(pos, w))
			
		# add os-dependent offset to point
		p = pos
		p += qcore.QPoint(2, 
						  21 if os.name == 'nt' else 16)
		
		# clamp within bounds
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(p.x() - 4 + self.width()) # XXX: p.rx()
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(p.y() - 24 + self.height()) # XXX: p.ry()
			
		if p.y() < screen.y():
			p.setY(screen.y())
			
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(screen.x() + screen.width() - self.width())
			
		if p.x() < screen.x():
			p.setX(screen.x())
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(screen.y() + screen.height() - self.height())
		
		# move label to this global point
		self.move(p)
	
	# ...........................
	
	# Reset/reseed the timers used for hiding the tooltips after a fixed time 
	def restartExpireTimer(self):
		time = 10000 + 40 * max(0, self.text().length()-100);
		self.expireTimer.start(time, self)
		self.hideTimer.stop()
	
	
	# Start fading out the tooltip bubble to hide it
	def hideTip(self):
		if not self.hideTimer.isActive():
			self.hideTimer.start(300, self)
	
	
	# Force tooltip bubble to hide immediately, without fade out delay
	def hideTipImmediately(self):
		self.close()        # to trigger QEvent::Close which stops the animation
		#self.deleteLater() # PORTINGFIX: disabled, as we may have more than one of these
	
	
	# ...........................
	
	# Set bounding rect and owner data for tooltip bubble
	# < w: (QWidget)
	# < r: (QRect)
	def setTipRect(self, w, r):
		if (not r.isNull()) and (not w):
			#qWarning("QToolTip::setTipRect: Cannot pass null widget if rect is set");
			print "QToolTip::setTipRect: Cannot pass null widget if rect is set"
		else:
			self.widget = w
			self.rect = r
	
	
	# Return which screen (on multi-screen setups) the tooltip appears on
	# < pos: (QPoint) point on screen where tooltip/widget appears
	# < widget: (QWidget) widget that tooltip appears for
	# > returns: (int) screen number/index
	#          >> -1 = if widget does not appear
	def getTipScreen(self, pos, widget):
		if qgui.QApplication.desktop().isVirtualDesktop():
			return qgui.QApplication.desktop().screenNumber(pos)
		else:
			return qgui.QApplication.desktop().screenNumber(widget)

####################################
