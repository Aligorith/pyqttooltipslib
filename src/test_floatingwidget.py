# This is a quick test of making floating widget appear/disappear on mouse over
# as if it were a tooltip. This file implements the test application for such
# a widget.	
#
# Author: Joshua Leung

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

import qfloatingwidget as QFW

#######################################

# Example of a custom widget which pops up 
class DummyTriggerWidget(qgui.QTextEdit):
	# ctor
	def __init__(self):
		qgui.QTextEdit.__init__(self)
		
		# setup floating widget
		self.make_floating_widget()
		self.setMouseTracking(True)
		
	# construct floating widget
	def make_floating_widget(self):
		# XXX: pass in self if not-windows
		self.floater = QFW.QFloatingWidget()
		
		layout = qgui.QVBoxLayout()
		self.floater.setLayout(layout)
		
		layout.addWidget(qgui.QLabel("This is a floater, with contents:"))
		layout.addWidget(qgui.QLineEdit())
		layout.addSpacing(20)
		layout.addWidget(qgui.QPushButton("Submit!"))
	
	# hide floater when leaving
	def leaveEvent(self, evt):
		self.floater.hideTip()
		
	# show floater when entering
	# XXX: abstract moving stuff out into a function
	def enterEvent(self, evt):
	#def mouseMoveEvent(self, evt):
		# XXX: need to move to position!
		self.floater.placeTip(qgui.QCursor.pos(), self)
		self.floater.show()

#######################################

# Test Application
class FloatingTestWin(qgui.QWidget):
	# ctor
	def __init__(self):
		qgui.QWidget.__init__(self)
		self.setWindowTitle("Floating Widget Test")
		
		# layout
		layout = qgui.QVBoxLayout()
		self.setLayout(layout)
		
		layout.addWidget(qgui.QLabel("Hover over the widget below to show floating widget:"))
		layout.addWidget(DummyTriggerWidget())
	
def main():
	app = qgui.QApplication([])

	win = FloatingTestWin() 
	win.show()

	app.exec_()

#######################################

if __name__ == '__main__':
	main()
